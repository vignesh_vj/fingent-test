import React from 'react'
import { Table } from 'react-bootstrap';

function TableCustom(props) {
  return (
    Array.isArray(props.data) && props.data.length > 0 ?
      <Table striped bordered hover variant="dark">
        <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Year</th>
            <th>imdbID </th>
          </tr>
        </thead>
        <tbody>

          {props.data.map((data, index) => {
            return (
              <tr key={index}
                onClick={() => {
                  window.location.href = `https://www.imdb.com/title/${data.imdbID}`
                }}
              >
                <td>{index}</td>
                <td>{data.Title}</td>
                <td>{data.Year}</td>
                <td>{data.imdbID}</td>
              </tr>
            )
          })}
        </tbody>
      </Table>
      : ''
  )
}


export default TableCustom;