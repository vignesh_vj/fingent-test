# Fingent - Test-1 REACT App 
  ### _Composed by Vignesh P. Vijay
## _Steps to reproduce_ 

 - Open the project directory inside VS Code (my personal preference).
 - Head over to the terminal by either pressing _Ctrl + Shift + `_ on the keyboard or by following _Terminal > New Terminal_
 - Run 
 ```sh
 npm i 
 npm start
 ``` 

## About the App

- The app has been built using REACT hooks (Functional components).
- The app has been bootstrapped using react-bootstrap package.
- The app primarily consists of two major components namely Home, and TableCustom whereas the former one acts as the parent component and the latter as the child component.
- API(GET) call to the provided url has been made in the parent component whereas the redirecting call has been made in the child component.


## Queries?
*** Feel free to contact me at +91 9946199405 
  
